/*
  Object Assign, Spread and Rest Operator
  Docs:
    https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

// Object.assign
// syntax: Object.assign(target, ...sources)
// var a = 'Карп',
//     b = "Селедка";
// var newStringLiteral = `${a} и ${b} сидели на трубe`;
//     console.log(newStringLiteral);
//
//     var d = ['a','b','c'];
//     var c = `
//     <div>
//       <ul>
//         ${
//           d.map( (item) => `<li>item</li>` )
//         }
//       </ul>
//     </div>
//     `;
//
//     console.log(c);

// let DataObj = {
//   data1: 'data1',
//   data2: 'data2'
// };
//
// let DataObj2 = {
//   data3: 'data3',
//   data4: 'data4'
// };


// let firstAssign = Object.assign(DataObj, DataObj2);
// console.log('firstAssign', firstAssign);
// console.log('DataObj', DataObj);
// // Изменяем значение исходного обьекта и проверяем значения обеих
// DataObj.data5 = 'data5';
// console.log('DataObj', DataObj);
// console.log('firstAssign', firstAssign);

// IMMUTABLE ASSIGN

// let secondAssign = Object.assign({}, DataObj, DataObj2 );
// console.log( 'secondAssign', secondAssign );
// DataObj.data6 = 'data6';
// console.log('DataObj - secondAssign', DataObj);
// console.log('secondAssign', secondAssign);
//
//
// let FunctionalObj = {
//   x: () => {
//     console.log('some important stuff');
//   },
//   y: {
//     a: 'a',
//     b: 'b',
//     c: 'c'
//   }
// };
//
// FunctionalObj.x();
// let thirdAssign = Object.assign({}, FunctionalObj);
// console.log( thirdAssign );
// thirdAssign.x();
//

// convert to obj
// var v1 = 'abcfadfsdfsds';
// var v2 = true;
// var v3 = 10;
// var v4 = Symbol('foo');
// var v5 = { value : true };
//
//
// var obj = Object.assign({}, v1, null, v2, undefined, v3, v4, v5);
// console.log(obj);


// var obj = {
//   foo: 1,
//   get bar() {
//     return 2;
//   }
// };
//
// obj.value = '';
// var copy = Object.assign({}, obj);
// console.log(copy);


// REST Operator

// in Function ->
// function RestTest(a, b, ...props){
//   console.log('a:', a, 'b', b, 'props', ...props);
// }
// var args = [0, 1, 2, 4, 5];
// RestTest.apply(null, args);



// // In array:
var iterableObj = ['i','t','e'];
var iterableObj2 = ['d','d','a'];
// var x = [ '4', 'five', 6, ...iterableObj, ...iterableObj2];
// console.log( 'rest in array:', x);


// concat arrays: old way;
var arr1 = [0, 1, 2];
var arr2 = [3, 4, 5];
// Append all items from arr2 onto arr1
// arr1 = arr1.concat(arr2);
// new way:
// arr1 = [...arr1, ...arr2];
// console.log( arr1 );
//
// // In obj
var obj = { value: 1};

// let objClone = { ...obj, ...DataObj, ...y };
// console.log( 'objClone', objClone );
//
let { ...z } = { 1: [], 2: [], a: 3, b: 4 };
// console.log(x); // 1
// console.log(k); // 2
console.log(z); // { a: 3, b: 4 }

// var sources = [{a: "A"}, {b: "B"}, {c: "C"}];
// options = Object.assign.apply(Object, [{}].concat(sources));
// console.log( options );
