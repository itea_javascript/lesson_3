// # TEMPLATE
( function MyApp(){
  // console.log('init function');
  // https://www.json-generator.com/#
  var fetchedData = fetch('http://www.json-generator.com/api/json/get/bUuBhzcCRe?indent=2').then(function(response) {
    return response.json();
  }).then(data => {
    renderInterface(data);
  });
}());

function renderInterface( data ){
  console.log('do some stuff with data:', data);
}

// Задача:
// Написать ф-ю которая принимает на вход обьект с сервера и
// разбить его на 3 массива по параметрам описаным ниже.
// + бонус вывести каждый список на экран
// + бонус 2 сделать поле инпута куда вставить ссылку с json-generator
// для перерендера списка по клику на кнопку
// + бонус 3 если вставить не валидную ссылку выводить ошибку

// # = условие -> вывод
// array 1 = fruit == banana -> name,
// array 2 = balance > 2000, age > 25
// array 3 = eyeColor === blue, gender === female, isActive === false
